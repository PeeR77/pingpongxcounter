TEMPLATE = subdirs
CONFIG += ordered
SUBDIRS = game_model \
          app \
          tests
app.depends = game_model
tests.depends = game_model
