QT += qml quick widgets

CONFIG += c++14

TARGET = PingPongXCounter

INCLUDEPATH += ../game_model/include

SOURCES += src/main.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../game_model/release/ -lgame_model
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../game_model/debug/ -lgame_model
else:unix: LIBS += -L$$OUT_PWD/../game_model -lgame_model

# Default rules for deployment.
include(deployment.pri)
