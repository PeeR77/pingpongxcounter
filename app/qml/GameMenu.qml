/*
 * Copyright (c) 2019 Paweł Rogoż.
 *
 * This file is part of PingPongXCounter 
 * (see https://gitlab.com/PeeR77/pingpongxcounter).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.12
import QtQuick.Controls 2.12

Item {
    signal buttonPressed()

    Column {
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            bottomMargin: 30
        }

        spacing: 10

        Button {
            id: restartSet
            anchors.horizontalCenter: parent.horizontalCenter

            text: qsTr("Restart set")
            onPressed: {
                buttonPressed()
                gameModel.resetSet()
            }
        }

        Button {
            id: restartMatch
            anchors.horizontalCenter: parent.horizontalCenter

            text: qsTr("Restart match")
            onPressed: {
                buttonPressed()
                gameModel.reset()
            }
        }

        Button {
            id: exit
            anchors.horizontalCenter: parent.horizontalCenter

            text: qsTr("Exit game")
            onPressed: {
                buttonPressed()
                gameModel.exitGame()
            }
        }
    }
}
