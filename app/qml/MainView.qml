/*
 * Copyright (c) 2019 Paweł Rogoż.
 *
 * This file is part of PingPongXCounter 
 * (see https://gitlab.com/PeeR77/pingpongxcounter).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import "./"

Item {
    id: mainView
    anchors.fill: parent

    readonly property bool portraitOrientation: parent.height > parent.width

    Connections {
        target: gameModel

        onGameStarted: {
            mainView.state = "playing";
        }

        onSetEnded: {
            mainView.state = "set_win";
        }

        onGameEnded: {
            mainView.state = "game_end";
        }

        onGameReseted: {
            mainView.state = "playing";
        }

        onGameWasUndo: {
            mainView.state = "playing";
        }
    }

    Image {
        id: background
        source: settings.backgroundFilePath

        anchors.fill: parent
        fillMode: Image.Stretch
    }

    ScoreButton {
        id: firstButton
        matchScore: gameModel.player1MatchScore
        setScore: gameModel.player1SetPoints
        serveActive: gameModel.servingPlayer === 1
        descriptionText: settings.player1Name

        anchors {
            horizontalCenter: portraitOrientation ? parent.horizontalCenter : undefined
            verticalCenter: portraitOrientation ? undefined : parent.verticalCenter
            left: portraitOrientation ? undefined : arrowGameMenu.right
            leftMargin: portraitOrientation ? undefined : 5
            top: portraitOrientation ? parent.top : undefined
            topMargin: portraitOrientation ? 10 : undefined
        }

        onClicked: gameModel.addPointPlayer1()
    }

    ScoreButton {
        id: secondButton
        matchScore: gameModel.player2MatchScore
        setScore: gameModel.player2SetPoints
        serveActive: gameModel.servingPlayer === 2
        descriptionText: settings.player2Name

        anchors {
            horizontalCenter: portraitOrientation ? parent.horizontalCenter : undefined
            verticalCenter: portraitOrientation ? undefined : parent.verticalCenter
            right: portraitOrientation ? undefined : arrowScoreTable.left
            rightMargin: portraitOrientation ? undefined : 5
            bottom: portraitOrientation ? parent.bottom : undefined
            bottomMargin: portraitOrientation ? 10 : undefined
        }

        onClicked: gameModel.addPointPlayer2()
    }

    StatusText {
        id: statusText
        anchors.centerIn: parent
        visible: false
    }

    RowLayout {
        spacing: 6

        anchors {
            horizontalCenter: parent.horizontalCenter
            top: statusText.bottom
            topMargin: 30
        }

        Button {
            id: resetButton
            text: qsTr("Reset game")
            visible: false

            onClicked: gameModel.reset()
        }

        Button {
            id: nextSetButton
            text: qsTr("Play next set")
            visible: false

            onClicked: gameModel.startNewSet()
        }
    }

    Button {
        id: undoButton
        text: qsTr("Undo")
        visible: (gameModel.player1MatchScore | gameModel.player2MatchScore |
                  gameModel.player1SetPoints | gameModel.player2SetPoints) != 0;

        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: statusText.top
            bottomMargin: 30
        }

        onClicked: gameModel.undo()
    }

    Drawer {
        id: drawerGameMenu
        width: mainView.width * 0.5
        height: mainView.height
        edge: Qt.LeftEdge
        dragMargin: gameMenuInfo.width + arrowGameMenu.width - 15
        interactive: mainView.visible

        GameMenu {
            anchors.fill: parent

            onButtonPressed: {
                drawerGameMenu.close()
            }
        }
    }

    Label {
        id: gameMenuInfo
        anchors {
            verticalCenter: parent.verticalCenter
            left: parent.left
            leftMargin: -25
        }

        rotation: 270
        text: qsTr("Game menu")
    }

    Label {
        id: arrowGameMenu

        anchors {
            verticalCenter: parent.verticalCenter
            left: gameMenuInfo.right
            leftMargin: -25
        }

        text: ">>"
    }

    Drawer {
        id: drawerScoreTable
        width: mainView.width * 0.9
        height: mainView.height
        edge: Qt.RightEdge
        dragMargin: scoreTableInfo.width + arrowScoreTable.width - 15
        interactive: mainView.visible

        SetScoreTable {
            anchors.fill: parent
        }
    }

    Label {
        id: scoreTableInfo
        anchors {
            verticalCenter: parent.verticalCenter
            right: parent.right
            rightMargin: -20
        }

        rotation: 270
        text: qsTr("Score table")
    }

    Label {
        id: arrowScoreTable

        anchors {
            verticalCenter: parent.verticalCenter
            right: scoreTableInfo.left
            rightMargin: -20
        }

        text: "<<"
    }

    states: [
        State {
            name: "set_win"
            PropertyChanges {
                target: statusText
                visible: true
                text: qsTr("Player %1  win set!!!".arg(gameModel.getSetWinningPlayerNumber()))
            }
            PropertyChanges {
                target: firstButton
                enabled: false
                color: gameModel.getSetWinningPlayerNumber() === 1 ? "green" : "red"
            }
            PropertyChanges {
                target: secondButton
                enabled: false
                color: gameModel.getSetWinningPlayerNumber() === 2 ? "green" : "red"
            }
            PropertyChanges {
                target: resetButton
                visible: true
            }
            PropertyChanges {
                target: nextSetButton
                visible: true
            }
        },

        State {
            name: "game_end"
            PropertyChanges {
                target: statusText
                visible: true
                text: qsTr("Player %1  win match!!!".arg(gameModel.getMatchWinningPlayerNumber()))
            }
            PropertyChanges {
                target: firstButton
                enabled: false
                color: gameModel.getMatchWinningPlayerNumber() === 1 ? "green" : "red"
            }
            PropertyChanges {
                target: secondButton
                enabled: false
                color: gameModel.getMatchWinningPlayerNumber() === 2 ? "green" : "red"
            }
        },

        State {
            name: "playing"
            PropertyChanges {
                target: statusText
                visible: false
                text: ""
            }
            PropertyChanges {
                target: firstButton
                enabled: true
                color: "white"
            }
            PropertyChanges {
                target: secondButton
                enabled: true
                color: "white"
            }
            PropertyChanges {
                target: resetButton
                visible: false
            }
        }
    ]

    onVisibleChanged: {
        gameModel.refreshGameScore();
    }

//    onStateChanged: {
//        console.log("state: " + state)
//    }
}

