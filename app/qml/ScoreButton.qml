/*
 * Copyright (c) 2019 Paweł Rogoż.
 *
 * This file is part of PingPongXCounter 
 * (see https://gitlab.com/PeeR77/pingpongxcounter).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.12
import QtQuick.Controls 2.12

Item {
    id: container
    property int setScore: 0
    property int matchScore: 0
    property bool serveActive: false
    property string descriptionText: ""
    property color color: "white"
    readonly property bool portraitOrientation: parent.height > parent.width
    signal clicked

    width: portraitOrientation ? (parent.width / 1.5) : (parent.width / 6)
    height: portraitOrientation ? (parent.height / 8) : (parent.height / 1.5)

    Button {
        id: button

        anchors.fill: parent
        onClicked: parent.clicked()

        Label {
            id: setScoreLabel
            anchors.centerIn: button
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: portraitOrientation ? button.height / 2 : button.width / 2
            text: setScore
        }

        Label {
            id: matchScoreLabel
            text: matchScore
            font.pixelSize: setScoreLabel.font.pixelSize / 2

            anchors {
                leftMargin: 5
                top: button.top
                left: button.left
            }
        }

        Label {
            id: playerNameLabel
            text: descriptionText
            font.pixelSize: setScoreLabel.font.pixelSize / 2

            anchors {
                rightMargin: 5
                bottom: button.bottom
                right: parent.right
            }
        }

        ServeInfo {
            id: serveInfo
            serveActive: container.serveActive

            anchors {
                left: parent.left
                verticalCenter: parent.verticalCenter
                leftMargin: 5
            }
        }
    }
}
