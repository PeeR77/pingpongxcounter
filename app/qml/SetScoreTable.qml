/*
 * Copyright (c) 2019 Paweł Rogoż.
 *
 * This file is part of PingPongXCounter
 * (see https://gitlab.com/PeeR77/pingpongxcounter).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.12
import QtQuick.Controls 2.12

Item {
    property int rowHeight: 25
    property int rowWidth: parent.width / 3 - 10
    property int viewSetIndex: matchHistoryBrowser.browseSetIndex

    onVisibleChanged: {
        matchHistoryBrowser.setIndexToCurrentSet()
    }

    Label {
        id: lSetInfo
        anchors {
            top: parent.top
            horizontalCenter: parent.horizontalCenter
            topMargin: 10
            leftMargin: 5
            rightMargin: 5
        }
        text: "Set: " + (viewSetIndex + 1) + "/" + settings.gameEnd
        font.pointSize: 20
    }

    Button {
        id: bSetPrevious

        anchors {
            verticalCenter: lSetInfo.verticalCenter
            right: lSetInfo.left
            rightMargin: 5
        }

        text: "<"

        enabled: viewSetIndex > 0
        onClicked: matchHistoryBrowser.browseSetIndex--
    }

    Button {
        id: bSetNext

        anchors {
            verticalCenter: lSetInfo.verticalCenter
            left: lSetInfo.right
            leftMargin: 5
        }

        text: ">"

        enabled: viewSetIndex < settings.gameEnd - 1
        onClicked: matchHistoryBrowser.browseSetIndex++
    }

    Grid {
        id: headerGrid
        columns: 3
        spacing: 10
        anchors {
            top: lSetInfo.bottom
            left: parent.left
            right: parent.right
            topMargin: 10
            leftMargin: 5
            rightMargin: 5
        }

        Rectangle {
            id: headerLp
            height: rowHeight
            width: rowWidth

            Text {
                anchors.centerIn: parent
                text: "No."
                font.pointSize: 20
            }
        }

        Rectangle {
            id: headerP1
            height: rowHeight
            width: rowWidth

            Text {
                anchors.centerIn: parent
                text: settings.player1Name
                font.pointSize: 20
                wrapMode: Text.WordWrap
            }
        }

        Rectangle {
            id: headerP2
            height: rowHeight
            width: rowWidth

            Text {
                anchors.centerIn: parent
                text: settings.player2Name
                font.pointSize: 20
                wrapMode: Text.WordWrap
            }
        }

        Rectangle {
            id: matchInfoRow
            height: rowHeight
            width: rowWidth

            Text {
                anchors.centerIn: parent
                text: qsTr("Match")
                font.pointSize: 20
            }
        }

        Item {
            id: matchPointsP1
            height: rowHeight
            width: rowWidth

            Rectangle {
                anchors {
                    fill: parent
                    leftMargin: 10
                    rightMargin: 10
                }
                radius: 5
                color: getPointsColor(gameModel.player1MatchScore, gameModel.player2MatchScore)

                Text {
                    anchors.centerIn: parent
                    text: gameModel.player1MatchScore
                    font.pointSize: 20
                }
            }
        }

        Item {
            id: matchPointsP2
            height: rowHeight
            width: rowWidth

            Rectangle {
                anchors {
                    fill: parent
                    leftMargin: 10
                    rightMargin: 10
                }
                radius: 5
                color: getPointsColor(gameModel.player2MatchScore, gameModel.player1MatchScore)
            }

            Text {
                anchors.centerIn: parent
                text: gameModel.player2MatchScore
                font.pointSize: 20
            }
        }

        Rectangle {
            id: setInfoRow
            height: rowHeight
            width: rowWidth

            Text {
                anchors.centerIn: parent
                text: qsTr("Set")
                font.pointSize: 20
            }
        }

        Rectangle {
            id: pointsP1
            height: rowHeight
            width: rowWidth
            color: getPointsColor(matchHistoryBrowser.player1SetPoints, matchHistoryBrowser.player2SetPoints)

            Text {
                anchors.centerIn: parent
                text: matchHistoryBrowser.player1SetPoints
                font.pointSize: 20
            }
        }

        Rectangle {
            id: pointsP2
            height: rowHeight
            width: rowWidth
            color: getPointsColor(matchHistoryBrowser.player2SetPoints, matchHistoryBrowser.player1SetPoints)

            Text {
                anchors.centerIn: parent
                text: matchHistoryBrowser.player2SetPoints
                font.pointSize: 20
            }
        }
    }

    ScrollView {
        id: scrollView
        anchors {
            top: headerGrid.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            topMargin: 10
            leftMargin: 5
            rightMargin: 5
        }

        Column {
            spacing: 6
            anchors.fill: parent

            Repeater {
                model: Math.max(matchHistoryBrowser.player1SetPoints + matchHistoryBrowser.player2SetPoints, settings.setEnd)
                Column {
                    spacing: 10
                    Row {
                        Rectangle {
                            height: rowHeight
                            width: scrollView.width / 3
                            Text {
                                anchors.centerIn: parent
                                text: index + 1
                                font.pointSize: 20
                            }
                        }

                        Rectangle {
                            height: rowHeight
                            width: scrollView.width / 3

                            ServeInfo {
                                anchors.centerIn: parent
                                size: 22
                                serveActive: matchHistoryBrowser.player1SetHistory[index]
                            }
                        }

                        Rectangle {
                            height: rowHeight
                            width: scrollView.width / 3

                            ServeInfo {
                                anchors.centerIn: parent
                                size: 22
                                serveActive: matchHistoryBrowser.player2SetHistory[index]
                            }
                        }
                    }
                }
            }
        }
    }

    function getPointsColor(pointsCurrent, pointsSeconds) {
        if(pointsCurrent > pointsSeconds) {
            return "green"
        } else if(pointsCurrent < pointsSeconds) {
            return "red"
        } else {
            return "yellow"
        }
    }
}
