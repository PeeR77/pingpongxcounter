/*
 * Copyright (c) 2019 Paweł Rogoż.
 *
 * This file is part of PingPongXCounter 
 * (see https://gitlab.com/PeeR77/pingpongxcounter).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Dialogs 1.2

Item {
    property alias bStartGame: bStartGame
    readonly property int labelsSize: 17
    readonly property int comboboxTextSize: 17
    readonly property int buttonTextSize: 16

    Grid {
        id: grid
        columns: 2
        spacing: 10
        horizontalItemAlignment: Grid.AlignRight
        verticalItemAlignment: Grid.AlignVCenter

        anchors {
            centerIn: parent
        }

        Label {
            id: lPlayer1
            font.pixelSize: labelsSize
            text: qsTr("Player 1 name:")
        }

        TextField {
            id: cbPlayer1
            text: settings.player1Name
            font.pixelSize: comboboxTextSize

            onTextChanged: {
                settings.player1Name = text
            }
        }

        Label {
            id: lPlayer2
            font.pixelSize: labelsSize
            text: qsTr("Player 2 name:")
        }

        TextField {
            id: cbPlayer2
            text: settings.player2Name
            font.pixelSize: comboboxTextSize

            onTextChanged: {
                settings.player2Name = text
            }
        }

        Label {
            id: lServe
            font.pixelSize: labelsSize
            text: qsTr("Serve start:")
        }

        ComboBox {
            id: cbServe
            model: ["Player 1", "Player 2"]
            currentIndex: settings.servingStartPlayer - 1
            font.pixelSize: comboboxTextSize

            onCurrentIndexChanged: {
                settings.servingStartPlayer = (currentIndex + 1)
            }
        }

        Label {
            id: lServeRandom
            font.pixelSize: labelsSize
            text: qsTr(enabled ? ("Drawn player: " + settings.servingStartPlayer) : " ")
            enabled: false
        }

        Button {
            id: bRandomServe
            font.pixelSize: buttonTextSize
            text: qsTr("Random")

            onPressed: {
                lServeRandom.enabled = false
                timerRandomDalay.start()
            }

            Timer {
                id: timerRandomDalay
                running: false
                repeat: false
                interval: 300

                onTriggered: {
                    settings.randomServe()
                    lServeRandom.enabled = true
                }
            }
        }

        Label {
            id: lgameType
            font.pixelSize: labelsSize
            text: qsTr("Set end:")
        }

        ComboBox {
            id: cbSetEnd
            model: [11, 21]
            currentIndex: getIndexFromCbModel(model, settings.setEnd)
            font.pixelSize: comboboxTextSize

            onCurrentIndexChanged: {
                settings.setEnd = model[currentIndex]
            }
        }

        Label {
            id: lGameEnd
            font.pixelSize: labelsSize
            text: qsTr("Game end:")
        }

        ComboBox {
            id: cbGameEnd
            model: [1, 3, 5, 7, 9]
            currentIndex: getIndexFromCbModel(model, settings.gameEnd)
            font.pixelSize: comboboxTextSize

            onCurrentIndexChanged: {
                settings.gameEnd = model[currentIndex]
            }
        }
    }

    Connections {
        target: settings
        onServingStartPlayerChanged: {
            cbServe.currentIndex = settings.servingStartPlayer - 1
        }
        onSetEndChanged: {
            cbSetEnd.currentIndex = getIndexFromCbModel(cbSetEnd.model, settings.setEnd)
        }
        onGameEndChanged: {
            cbGameEnd.currentIndex = getIndexFromCbModel(cbGameEnd.model, settings.gameEnd)
        }
    }

    Column {
            anchors {
                top: grid.bottom
                horizontalCenter: parent.horizontalCenter
                topMargin: 15
            }

        Label {
            id: lbackground
            font.pixelSize: labelsSize
            text: qsTr("Background file:")
        }

        Row {
            spacing: 6

            TextField {
                id: tfBackgoundFilePath
                text: settings.backgroundFilePath

                onTextChanged: {
                    settings.backgroundFilePath = text
                }
            }

            Button {
                id: bFindFile
                font.pixelSize: buttonTextSize
                text: qsTr("...")

                onPressed: {
                    fileDialog.visible = true
                }
            }
        }
    }

    Row {
        anchors {
            bottom: parent.bottom
            horizontalCenter: parent.horizontalCenter
            bottomMargin: 10
        }

        spacing: 6

        Button {
            id: bStartGame
            font.pixelSize: buttonTextSize
            text: qsTr("Start game")
        }

        Button {
            id: bResetToDefault
            font.pixelSize: buttonTextSize
            text: qsTr("Reset to default")

            onClicked: {
                settings.setDefaultSettings();
            }
        }
    }

    FileDialog {
        id: fileDialog
        title: "Please choose a backgound file"
        folder: shortcuts.home
        nameFilters: [ "Image files (*.jpg *.png)", "All files (*)" ]
        visible: false

        onAccepted: {
            tfBackgoundFilePath.text = fileDialog.fileUrl
        }
    }

    Component.onDestruction: {
        settings.saveSettings();
    }

    function getIndexFromCbModel(model, value) {
        return model.indexOf(value);
    }
}
