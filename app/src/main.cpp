/*
 * Copyright (c) 2019 Paweł Rogoż.
 *
 * This file is part of PingPongXCounter 
 * (see https://gitlab.com/PeeR77/pingpongxcounter).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>

#include "settings.h"
#include "gamemodel.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);

    app.setOrganizationName(Settings::kOrganizationName);
    app.setOrganizationDomain("pingpongxcounter.com");
    app.setApplicationName(Settings::kApplicationName);

    qmlRegisterType<GameModel>("PPXCounter", 1, 0, "GameModel");

    QQmlApplicationEngine engine;
    GameModel gameModel;
    auto& matchHistoryBrowser = gameModel.getMatchHistoryBrowser();

    engine.rootContext()->setContextProperty("gameModel", &gameModel);
    engine.rootContext()->setContextProperty("matchHistoryBrowser", &matchHistoryBrowser);
    engine.rootContext()->setContextProperty("settings", Settings::getInstance());
    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

    return app.exec();
}
