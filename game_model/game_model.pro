#-------------------------------------------------
#
# Project created by QtCreator 2016-07-10T14:48:38
#
#-------------------------------------------------

QT       += qml quick

QT       -= gui

TARGET = game_model
TEMPLATE = lib
CONFIG += staticlib c++14

SOURCES += src/gamemodel.cpp \
    src/matchhistorybrowser.cpp \
    src/gamedata.cpp \
    src/settings.cpp

INCLUDEPATH += ./include

HEADERS += include/gamemodel.h \
    include/gamedata.h \
    include/matchhistorybrowser.h \
    include/settings.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
