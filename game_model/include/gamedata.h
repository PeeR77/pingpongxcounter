/*
 * Copyright (c) 2019 Paweł Rogoż.
 *
 * This file is part of PingPongXCounter 
 * (see https://gitlab.com/PeeR77/pingpongxcounter).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef GAMEDATA_H
#define GAMEDATA_H

#include <QObject>
#include <QList>

enum Player { Player1 = 1, Player2 = 2 };

struct SetData
{
    int m_player1Score = 0;
    int m_player2Score = 0;
    QList<Player> m_scoreHistory;

    void addPoint(Player player);
    void undoPoint(Player player);
    Player getLastPlayerScore() const { return m_scoreHistory.constLast(); }
};

struct GameData
{
    int m_player1MatchScore = 0;
    int m_player2MatchScore = 0;
    QVector<SetData> m_matchData{SetData()};

public:
    const SetData& getCurrentSet() const { return m_matchData.last(); }
    SetData& getCurrentSet() { return m_matchData.last(); }
    int getPlayerScoreInCurrentSet(Player player);
    void addMatchScore(Player player);
    void startNewSet();
    void undoSet();
    void resetSet();
    void reset();

private:
    void undoMatchScore(Player player);
};

#endif // GAMEDATA_H
