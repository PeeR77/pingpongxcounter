/*
 * Copyright (c) 2019 Paweł Rogoż.
 *
 * This file is part of PingPongXCounter 
 * (see https://gitlab.com/PeeR77/pingpongxcounter).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef GAMEMODEL_H
#define GAMEMODEL_H

#include <QObject>
#include "gamedata.h"
#include "matchhistorybrowser.h"
#include "settings.h"

class GameModel : public QObject
{
    Q_OBJECT

    Q_ENUM(Player);
    Q_PROPERTY(int player1MatchScore READ getPlayer1MatchScore NOTIFY playerMatchScoreChanged)
    Q_PROPERTY(int player2MatchScore READ getPlayer2MatchScore NOTIFY playerMatchScoreChanged)
    Q_PROPERTY(int player1SetPoints READ getPlayer1SetPoints NOTIFY playerPointsChanged)
    Q_PROPERTY(int player2SetPoints READ getPlayer2SetPoints NOTIFY playerPointsChanged)
    Q_PROPERTY(int servingPlayer READ getServingPlayer NOTIFY servingPlayerChanged)
    Q_PROPERTY(int currentSetNumber READ getCurrentSetNumber NOTIFY currentSetNumberChanged)

public:
    explicit GameModel(QObject *parent = nullptr);

public:
    int getPlayer1MatchScore() const;
    int getPlayer2MatchScore() const;
    int getPlayer1SetPoints() const;
    int getPlayer2SetPoints() const;
    bool isPlayer1Won() const;
    bool isPlayer2Won() const;
    int getServingPlayer() const;
    int getCurrentSetNumber() const;
    MatchHistoryBrowser& getMatchHistoryBrowser() { return m_matchHistoryBrowser; }

signals:
    void gameStarted();
    void gameExited();
    void setEnded(Player win);
    void gameEnded();
    void gameReseted();
    void gameWasUndo();

    void playerMatchScoreChanged(Player player);
    void playerPointsChanged(Player player);
    void pointScored();

    void servingPlayerChanged();
    void currentSetNumberChanged(int currentSetNumber);

public slots:
    Q_INVOKABLE void exitGame();
    Q_INVOKABLE void reset();
    Q_INVOKABLE void resetSet();
    Q_INVOKABLE int getSetWinningPlayerNumber() const;
    Q_INVOKABLE int getMatchWinningPlayerNumber() const;
    Q_INVOKABLE void addPointPlayer1();
    Q_INVOKABLE void addPointPlayer2();
    Q_INVOKABLE void undo();
    Q_INVOKABLE void startNewSet();
    Q_INVOKABLE void refreshGameScore();

private:
    Player getSetWinningPlayer() const;
    Player getMatchWinningPlayer() const;
    bool isSetEnd() const;
    bool isGameEnd() const;
    void addPoint(Player playerWhoScored);
    int getPlayerSetScore(Player player) const;


private:
    GameData gameData;
    MatchHistoryBrowser m_matchHistoryBrowser{gameData, this};
    Settings* settings = Settings::getInstance();
};

#endif // GAMEMODEL_H
