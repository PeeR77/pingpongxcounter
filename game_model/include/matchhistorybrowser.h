/*
 * Copyright (c) 2019 Paweł Rogoż.
 *
 * This file is part of PingPongXCounter 
 * (see https://gitlab.com/PeeR77/pingpongxcounter).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MATCHHISTORYBROWSER_H
#define MATCHHISTORYBROWSER_H

#include <QObject>

#include "gamedata.h"
#include "settings.h"


class MatchHistoryBrowser : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int browseSetIndex READ getBrowseSetIndex WRITE setBrowseSetIndex NOTIFY browseSetIndexChanged)
    Q_PROPERTY(int player1SetPoints READ getPlayer1SetPoints NOTIFY player1PointsChanged)
    Q_PROPERTY(int player2SetPoints READ getPlayer2SetPoints NOTIFY player2PointsChanged)
    Q_PROPERTY(QVector<bool> player1SetHistory READ getPlayer1SetHistory NOTIFY player1SetHistoryChanged)
    Q_PROPERTY(QVector<bool> player2SetHistory READ getPlayer2SetHistory NOTIFY player2SetHistoryChanged)

public:
    explicit MatchHistoryBrowser(GameData& gameData, QObject *parent = nullptr);

    int getPlayer1SetPoints() const;
    int getPlayer2SetPoints() const;

    QVector<bool> getPlayer1SetHistory() const
    {
         return generatePlayerSetHistory(Player1, m_browseSetIndex);
    }

    QVector<bool> getPlayer2SetHistory() const
    {
        return generatePlayerSetHistory(Player2, m_browseSetIndex);
    }

    int getBrowseSetIndex() const
    {
        return m_browseSetIndex;
    }

signals:
    void player1PointsChanged(int player1SetPoints);
    void player2PointsChanged(int player2SetPoints);
    void player1SetHistoryChanged(QVector<bool> player1SetHistory);
    void player2SetHistoryChanged(QVector<bool> player2SetHistory);
    void browseSetIndexChanged(int browseSetIndex);

public slots:
    void setBrowseSetIndex(int index);
    void updateModel();
    void setIndexToCurrentSet();

private:
    SetData getSetData(int index) const;
    QVector<bool> generatePlayerSetHistory(Player player, int index) const;
    int getPlayer1SetPointsByIndex(int index) const;
    int getPlayer2SetPointsByIndex(int index) const;

private:
    GameData& m_gameData;
    Settings* settings = Settings::getInstance();
    int m_browseSetIndex = 0;
};

#endif // MATCHHISTORYBROWSER_H
