/*
 * Copyright (c) 2019 Paweł Rogoż.
 *
 * This file is part of PingPongXCounter 
 * (see https://gitlab.com/PeeR77/pingpongxcounter).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef INCLUDESETTINGS_H
#define INCLUDESETTINGS_H

#include <QObject>

class Settings : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString player1Name READ getPlayer1Name WRITE setPlayer1Name NOTIFY player1NameChanged)
    Q_PROPERTY(QString player2Name READ getPlayer2Name WRITE setPlayer2Name NOTIFY player2NameChanged)
    Q_PROPERTY(int servingStartPlayer READ getServingStartPlayer WRITE setServingStartPlayer NOTIFY servingStartPlayerChanged)
    Q_PROPERTY(int setEnd READ getSetEnd WRITE setSetEnd NOTIFY setEndChanged)
    Q_PROPERTY(int gameEnd READ getGameEnd WRITE setGameEnd NOTIFY gameEndChanged)
    Q_PROPERTY(QString backgroundFilePath READ getBackgroundFilePath WRITE setBackgroundFilePath NOTIFY backgroundFilePathChanged)

public:
    static constexpr char kOrganizationName[] = "PeeR Soft";
    static constexpr char kApplicationName[] = "Ping Pong XCounter";

    static constexpr char kDefaultPlayer1Name[] = "Player 1";
    static constexpr char kDefaultPlayer2Name[] = "Player 2";
    static constexpr int kDefaultServingStartPlayer = 1;
    static constexpr int kDefaulSetEnd= 11;
    static constexpr int kDefaulteGameEnd = 5;

public:
    static Settings* getInstance() {
        static Settings instance;
        return &instance;
    }

private:
    explicit Settings(QObject *parent = nullptr);

signals:
    void player1NameChanged();
    void player2NameChanged();
    void setEndChanged();
    void gameEndChanged();
    void servingStartPlayerChanged();
    void backgroundFilePathChanged();

public slots:
    QString getPlayer1Name() const;
    void setPlayer1Name(const QString &player1Name);
    QString getPlayer2Name() const;
    void setPlayer2Name(const QString &player2Name);
    int getServingStartPlayer() const;
    void setServingStartPlayer(int servingStartPlayer);
    int getSetEnd() const;
    void setSetEnd(int setEnd);
    int getGameEnd() const;
    void setGameEnd(int gameEnd);
    void saveSettings();

    QString getBackgroundFilePath() const;
    void setBackgroundFilePath(const QString &backgroundFilePath);

    Q_INVOKABLE void setDefaultSettings();
    Q_INVOKABLE void randomServe();

private:
    void loadSettings();
    bool fileExists(QString path) const;

private:
    QString m_player1Name;
    QString m_player2Name;
    int m_servingStartPlayer = 1;
    int m_setEnd = 11;
    int m_gameEnd = 5;

    QString m_backgroundFilePath;
};

#endif // INCLUDESETTINGS_H
