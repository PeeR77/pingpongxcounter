/*
 * Copyright (c) 2019 Paweł Rogoż.
 *
 * This file is part of PingPongXCounter 
 * (see https://gitlab.com/PeeR77/pingpongxcounter).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "gamedata.h"
#include <QDebug>


int GameData::getPlayerScoreInCurrentSet(Player player)
{
    switch (player) {
    case Player1:
        return getCurrentSet().m_player1Score;
    case Player2:
        return getCurrentSet().m_player2Score;
    default:
        Q_ASSERT_X(true, __PRETTY_FUNCTION__, "No player");
        break;
    }
    return -1;
}

void GameData::addMatchScore(Player player)
{
    switch (player) {
    case Player1:
        m_player1MatchScore++;
        break;
    case Player2:
        m_player2MatchScore++;
        break;
    default:
        qDebug() << "Error in addMatchScore no player";
        break;
    }
}

void GameData::startNewSet()
{
    m_matchData.append(SetData());
    getCurrentSet() = m_matchData.last();
}

void GameData::undoSet()
{
    if (m_matchData.isEmpty()) {
        return;
    }

    m_matchData.removeLast();
    getCurrentSet() = m_matchData.last();
    undoMatchScore(getCurrentSet().getLastPlayerScore());
    getCurrentSet().undoPoint(getCurrentSet().getLastPlayerScore());
}

void GameData::resetSet()
{
    getCurrentSet().m_player1Score = 0;
    getCurrentSet().m_player2Score = 0;
    getCurrentSet().m_scoreHistory.clear();
}

void GameData::reset()
{
    m_matchData.clear();
    m_player1MatchScore = m_player2MatchScore = 0;
    startNewSet();
}

void GameData::undoMatchScore(Player player)
{
    switch (player) {
    case Player1:
        m_player1MatchScore--;
        break;
    case Player2:
        m_player2MatchScore--;
        break;
    default:
        qDebug() << "Error in undoMatchScore no player";
        break;
    }
}

void SetData::addPoint(Player player)
{
    switch (player) {
    case Player1:
        m_player1Score++;
        break;
    case Player2:
        m_player2Score++;
        break;
    default:
        qDebug() << "Error: GameData::addPoint wrong playerScore: " << player;
        Q_ASSERT(0);
        break;
    }

    m_scoreHistory.append(player);
}

void SetData::undoPoint(Player player)
{
    switch (player) {
    case Player1:
        m_player1Score--;
        break;
    case Player2:
        m_player2Score--;
        break;
    default:
        Q_ASSERT(0);
        break;
    }

    m_scoreHistory.removeLast();
}
