/*
 * Copyright (c) 2019 Paweł Rogoż.
 *
 * This file is part of PingPongXCounter 
 * (see https://gitlab.com/PeeR77/pingpongxcounter).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "gamemodel.h"
#include <QDebug>
#include <QtMath>

GameModel::GameModel(QObject *parent) : QObject(parent)
{
    connect(this, &GameModel::playerPointsChanged, &GameModel::pointScored);
    connect(this, &GameModel::pointScored, &m_matchHistoryBrowser, &MatchHistoryBrowser::updateModel);
}

int GameModel::getPlayer1MatchScore() const
{
    return gameData.m_player1MatchScore;
}

int GameModel::getPlayer2MatchScore() const
{
    return gameData.m_player2MatchScore;
}

int GameModel::getPlayer1SetPoints() const
{
    return gameData.getCurrentSet().m_player1Score;
}

int GameModel::getPlayer2SetPoints() const
{
    return gameData.getCurrentSet().m_player2Score;
}

bool GameModel::isPlayer1Won() const
{
    return isSetEnd() && (getSetWinningPlayer() == Player1);
}

bool GameModel::isPlayer2Won() const
{
    return isSetEnd() && (getSetWinningPlayer() == Player2);
}

Player GameModel::getSetWinningPlayer() const
{
    return getPlayer1SetPoints() > getPlayer2SetPoints() ?
                Player1 : Player2;
}

Player GameModel::getMatchWinningPlayer() const
{
    return getPlayer1SetPoints() > getPlayer2SetPoints() ?
                Player1 : Player2;
}

int GameModel::getServingPlayer() const
{
    int currentServingPlayer;
    Player matchFirstServingPlayer = static_cast<Player>(settings->getServingStartPlayer());
    Player matchSecondServingPlayer = matchFirstServingPlayer == Player1 ? Player2 : Player1;
    Player setFirstServingPlayer = (getCurrentSetNumber() % 2) == 0 ?
                matchSecondServingPlayer : matchFirstServingPlayer;
    Player setSecondServingPlayer = (setFirstServingPlayer == Player1) ? Player2 : Player1;

    bool passWinningBarier = (gameData.getCurrentSet().m_player1Score >= settings->getSetEnd() ||
            gameData.getCurrentSet().m_player2Score >= settings->getSetEnd());
    int mod = passWinningBarier ? 2 : 4;
    int sum_mod = (gameData.getCurrentSet().m_player1Score +
                   gameData.getCurrentSet().m_player2Score) % mod;

    if(passWinningBarier) {
        if (sum_mod == 0) {
            currentServingPlayer = setFirstServingPlayer;
        } else {
            currentServingPlayer = setSecondServingPlayer;
        }
    } else {
        if (sum_mod == 0 || sum_mod == 1) {
            currentServingPlayer = setFirstServingPlayer;
        } else {
            currentServingPlayer = setSecondServingPlayer;
        }
    }

    return currentServingPlayer;
}

int GameModel::getCurrentSetNumber() const
{
    return gameData.m_player1MatchScore + gameData.m_player2MatchScore + 1;
}

void GameModel::exitGame()
{
    reset();
    emit gameExited();
}

void GameModel::reset()
{
    gameData.reset();

    emit gameReseted();
    refreshGameScore();
}

void GameModel::resetSet()
{
    gameData.resetSet();
    refreshGameScore();
    emit gameWasUndo();
}

int GameModel::getSetWinningPlayerNumber() const
{
    return getSetWinningPlayer();
}

int GameModel::getMatchWinningPlayerNumber() const
{
    return getMatchWinningPlayer();
}

void GameModel::addPointPlayer1()
{
    addPoint(Player1);
}

void GameModel::addPointPlayer2()
{
    addPoint(Player2);
}

void GameModel::addPoint(Player playerWhoScored)
{
    gameData.getCurrentSet().addPoint(playerWhoScored);
    refreshGameScore();

    if (isSetEnd()) {
        if(isGameEnd()) {
            emit gameEnded();
            return;
        }

        emit setEnded(getSetWinningPlayer());
    }
}

void GameModel::undo()
{
    if(gameData.getCurrentSet().m_player1Score == 0 &&
            gameData.getCurrentSet().m_player2Score == 0) {
        if((gameData.m_player1MatchScore | gameData.m_player2MatchScore) > 0) {
            gameData.undoSet();
        } else {
            Q_ASSERT(0);
        }
    } else {
        gameData.getCurrentSet().undoPoint(gameData.getCurrentSet().getLastPlayerScore());
    }

    refreshGameScore();
    emit gameWasUndo();
}

void GameModel::startNewSet()
{
    gameData.addMatchScore(gameData.getCurrentSet().getLastPlayerScore());
    gameData.startNewSet();

    refreshGameScore();
    emit gameStarted();
}

bool GameModel::isSetEnd() const
{
    int player_score = getPlayerSetScore(getSetWinningPlayer());
    if(player_score >= settings->getSetEnd() &&
            qAbs(getPlayer1SetPoints() - getPlayer2SetPoints()) > 1) {
        return true;
    }

    return false;
}

bool GameModel::isGameEnd() const
{
    int player1MatchScore = getPlayer1MatchScore();
    int player2MatchScore = getPlayer2MatchScore();
    if(isSetEnd()) { // match point from current set not yet added
        Player winningPlayer = getSetWinningPlayer();
        if(winningPlayer == Player::Player1) {
            player1MatchScore++;
        } else if(winningPlayer == Player::Player2) {
            player2MatchScore++;
        }
    }

    int playersHighestScore = qMax(player1MatchScore, player2MatchScore);
    int minimumMatchScoreToWin = qCeil(settings->getGameEnd() / 2.0);

    return playersHighestScore >= minimumMatchScoreToWin;
}

int GameModel::getPlayerSetScore(Player player) const
{
    switch (player) {
    case Player1:
        return getPlayer1SetPoints();
    case Player2:
        return getPlayer2SetPoints();
    default:
        Q_ASSERT(0);
        return -1;
    }
}

void GameModel::refreshGameScore()
{
    emit playerMatchScoreChanged(Player::Player1);
    emit playerMatchScoreChanged(Player::Player2);
    emit playerPointsChanged(Player::Player1);
    emit playerPointsChanged(Player::Player2);
    emit servingPlayerChanged();
    emit currentSetNumberChanged(getCurrentSetNumber());
}
