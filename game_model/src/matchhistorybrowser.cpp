/*
 * Copyright (c) 2019 Paweł Rogoż.
 *
 * This file is part of PingPongXCounter 
 * (see https://gitlab.com/PeeR77/pingpongxcounter).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include "matchhistorybrowser.h"

MatchHistoryBrowser::MatchHistoryBrowser(GameData& gameData, QObject *parent) : QObject(parent),
    m_gameData(gameData)
{
}

int MatchHistoryBrowser::getPlayer1SetPoints() const
{
    return getPlayer1SetPointsByIndex(m_browseSetIndex);
}

int MatchHistoryBrowser::getPlayer2SetPoints() const
{
    return getPlayer2SetPointsByIndex(m_browseSetIndex);
}

void MatchHistoryBrowser::setBrowseSetIndex(int index)
{
    if(m_browseSetIndex == index)
        return;

    m_browseSetIndex = index;
    emit browseSetIndexChanged(m_browseSetIndex);
    updateModel();
}

void MatchHistoryBrowser::updateModel()
{
    emit player1PointsChanged(getPlayer1SetPoints());
    emit player2PointsChanged(getPlayer2SetPoints());
    emit player1SetHistoryChanged(getPlayer1SetHistory());
    emit player2SetHistoryChanged(getPlayer2SetHistory());
}

void MatchHistoryBrowser::setIndexToCurrentSet()
{
    setBrowseSetIndex(m_gameData.m_matchData.size() - 1);
}

SetData MatchHistoryBrowser::getSetData(int index) const
{
    if(index < m_gameData.m_matchData.size())
        return m_gameData.m_matchData[index];

    return SetData();
}

QVector<bool> MatchHistoryBrowser::generatePlayerSetHistory(Player player, int index) const
{
    const auto& scoreHistory = getSetData(index).m_scoreHistory;
    QVector<bool> historyVect(qMax(settings->getSetEnd(), scoreHistory.count()), false);

    for(int i = 0; i < scoreHistory.count(); i++) {
        historyVect[i] = (scoreHistory[i] == player);
    }

    return historyVect;
}

int MatchHistoryBrowser::getPlayer1SetPointsByIndex(int index) const
{
    if (index < m_gameData.m_matchData.size())
        return m_gameData.m_matchData[index].m_player1Score;

    return 0;
}

int MatchHistoryBrowser::getPlayer2SetPointsByIndex(int index) const
{
    if (index < m_gameData.m_matchData.size())
        return m_gameData.m_matchData[index].m_player2Score;

    return 0;
}
