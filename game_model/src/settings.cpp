/*
 * Copyright (c) 2019 Paweł Rogoż.
 *
 * This file is part of PingPongXCounter 
 * (see https://gitlab.com/PeeR77/pingpongxcounter).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include "settings.h"

#include <QFileInfo>
#include <QRandomGenerator>
#include <QSettings>

constexpr char Settings::kOrganizationName[];
constexpr char Settings::kApplicationName[];
constexpr char Settings::kDefaultPlayer1Name[];
constexpr char Settings::kDefaultPlayer2Name[];

Settings::Settings(QObject *parent) : QObject(parent)
{
    loadSettings();
}

void Settings::setDefaultSettings()
{
    setPlayer1Name(kDefaultPlayer1Name);
    setPlayer2Name(kDefaultPlayer2Name);
    setServingStartPlayer(kDefaultServingStartPlayer);
    setSetEnd(kDefaulSetEnd);
    setGameEnd(kDefaulteGameEnd);
    setBackgroundFilePath("");
}

void Settings::randomServe()
{
    int random = QRandomGenerator::global()->generate() % 2;
    setServingStartPlayer(random == 0 ? 1 : 2);
}

void Settings::saveSettings()
{
    QSettings settings;

    settings.beginGroup("Game");
    settings.setValue("player1Name", m_player1Name);
    settings.setValue("player2Name", m_player2Name);
    settings.setValue("servingStartPlayer", m_servingStartPlayer);
    settings.setValue("setEnd", m_setEnd);
    settings.setValue("gameEnd", m_gameEnd);
    settings.endGroup();

    settings.beginGroup("Window");
    settings.setValue("backgroundFilePath", m_backgroundFilePath);
    settings.endGroup();
}

void Settings::loadSettings()
{
    QSettings settings(kOrganizationName, kApplicationName);

    settings.beginGroup("Game");
    m_player1Name = settings.value("player1Name", kDefaultPlayer1Name).toString();
    m_player2Name = settings.value("player2Name", kDefaultPlayer2Name).toString();
    m_servingStartPlayer = settings.value("servingStartPlayer", kDefaultServingStartPlayer).toInt();
    m_setEnd = settings.value("setEnd", kDefaulSetEnd).toInt();
    m_gameEnd = settings.value("gameEnd", kDefaulteGameEnd).toInt();
    settings.endGroup();

    settings.beginGroup("Window");
    m_backgroundFilePath = settings.value("backgroundFilePath", "").toString();
    if(!fileExists(m_backgroundFilePath)) {
        m_backgroundFilePath = "";
    }
    settings.endGroup();
}

bool Settings::fileExists(QString path) const
{
    path.replace("file:///", "");
    QFileInfo checkFile(path);
    return checkFile.exists() && checkFile.isFile();
}

QString Settings::getPlayer2Name() const
{
    return m_player2Name;
}

void Settings::setPlayer2Name(const QString &player2Name)
{
    if (m_player2Name == player2Name)
        return;

    m_player2Name = player2Name;
    emit player2NameChanged();
}

QString Settings::getPlayer1Name() const
{
    return m_player1Name;
}

void Settings::setPlayer1Name(const QString &player1Name)
{
    if (m_player1Name == player1Name)
        return;

    m_player1Name = player1Name;
    emit player1NameChanged();
}

int Settings::getServingStartPlayer() const
{
    return m_servingStartPlayer;
}

void Settings::setServingStartPlayer(int servingStartPlayer)
{
    if (m_servingStartPlayer == servingStartPlayer)
        return;

    m_servingStartPlayer = servingStartPlayer;
    emit servingStartPlayerChanged();
}

int Settings::getSetEnd() const
{
    return m_setEnd;
}

void Settings::setSetEnd(int setEnd)
{
    if (m_setEnd == setEnd)
        return;

    m_setEnd = setEnd;
    emit setEndChanged();
}

int Settings::getGameEnd() const
{
    return m_gameEnd;
}

void Settings::setGameEnd(int gameEnd)
{
    if (m_gameEnd == gameEnd)
        return;

    m_gameEnd = gameEnd;
    emit gameEndChanged();
}

QString Settings::getBackgroundFilePath() const
{
    return m_backgroundFilePath;
}

void Settings::setBackgroundFilePath(const QString &backgroundFilePath)
{
    if (m_backgroundFilePath == backgroundFilePath)
        return;

    m_backgroundFilePath = backgroundFilePath;
    emit backgroundFilePathChanged();
}
