#-------------------------------------------------
#
# Project created by QtCreator 2015-08-29T20:40:42
#
#-------------------------------------------------

QT       += testlib

QT       -= gui

TARGET = tst_gamemodeltest
CONFIG += console
CONFIG += c++14
CONFIG -= app_bundle

TEMPLATE = app

PROJECT_PATH = ../..
INCLUDEPATH += $${PROJECT_PATH}/game_model/include

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../game_model/release/ -lgame_model
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../game_model/debug/ -lgame_model
else:unix: LIBS += -L$$OUT_PWD/../../game_model -lgame_model

SOURCES += \
    main.cpp \
    tst_gamemodel.cpp \
    tst_matchhistorybrowser.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    tst_gamemodel.h \
    tst_matchhistorybrowser.h


