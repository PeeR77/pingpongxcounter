/*
 * Copyright (c) 2019 Paweł Rogoż.
 *
 * This file is part of PingPongXCounter 
 * (see https://gitlab.com/PeeR77/pingpongxcounter).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "tst_gamemodel.h"


void GameModelTest::incrementPoints(int points, PlayerIncrement incrementMode)
{
    for (int i = 0; i < points; i++) {
        if (incrementMode == PlayerIncrement::Player1 || incrementMode == PlayerIncrement::Both) {
            gm->addPointPlayer1();
        }
        if (incrementMode == PlayerIncrement::Player2 || incrementMode == PlayerIncrement::Both) {
            gm->addPointPlayer2();
        }
    }
}

void GameModelTest::VERIFY_PLAYER_1_WIN(bool reallyWin)
{
    if(reallyWin) {
        QVERIFY2(gm->isPlayer1Won(), "Player 1 should win");
    } else {
        QVERIFY2(!gm->isPlayer1Won(), "Player 1 shouldn't' win");
    }
}

void GameModelTest::VERIFY_PLAYER_2_WIN(bool reallyWin)
{
    if(reallyWin) {
        QVERIFY2(gm->isPlayer2Won(), "Player 2 should win");
    } else {
        QVERIFY2(!gm->isPlayer2Won(), "Player 2 shouldn't' win");
    }
}

void GameModelTest::player1Win11To0Test()
{
    incrementPoints(WHEN_SET_END, PlayerIncrement::Player1);

    VERIFY_PLAYER_1_WIN();
    VERIFY_PLAYER_2_WIN(false);
}

void GameModelTest::player2Win11To0Test()
{
    incrementPoints(WHEN_SET_END, PlayerIncrement::Player2);

    VERIFY_PLAYER_2_WIN();
    VERIFY_PLAYER_1_WIN(false);
}

void GameModelTest::SetPlayer1_11ToPlayer2_10Test()
{
    incrementPoints(WHEN_SET_END - 1, PlayerIncrement::Player2);
    incrementPoints(WHEN_SET_END, PlayerIncrement::Player1);

    VERIFY_PLAYER_1_WIN(false);
    VERIFY_PLAYER_2_WIN(false);
}

void GameModelTest::SetPlayer2_11ToPlayer1_10Test()
{
    incrementPoints(WHEN_SET_END - 1, PlayerIncrement::Player1);
    incrementPoints(WHEN_SET_END, PlayerIncrement::Player2);

    VERIFY_PLAYER_1_WIN(false);
    VERIFY_PLAYER_2_WIN(false);
}

void GameModelTest::winOnMajorityPlayer1Test()
{
    incrementPoints(WHEN_SET_END, PlayerIncrement::Both);

    VERIFY_PLAYER_1_WIN(false);
    gm->addPointPlayer1();
    VERIFY_PLAYER_1_WIN(false);
    gm->addPointPlayer1();
    VERIFY_PLAYER_1_WIN();
}

void GameModelTest::winOnMajorityPlayer2Test()
{
    incrementPoints(WHEN_SET_END, PlayerIncrement::Both);

    VERIFY_PLAYER_2_WIN(false);
    gm->addPointPlayer2();
    VERIFY_PLAYER_2_WIN(false);
    gm->addPointPlayer2();
    VERIFY_PLAYER_2_WIN();
}

void GameModelTest::winOnMajorityWithUndo()
{
    winOnMajorityPlayer1Test();

    gm->undo();
    VERIFY_PLAYER_1_WIN(false);
    gm->addPointPlayer2();
    VERIFY_PLAYER_2_WIN(false);
    gm->addPointPlayer2();
    VERIFY_PLAYER_2_WIN(false);
    gm->addPointPlayer2();
    VERIFY_PLAYER_2_WIN();
}

void GameModelTest::winOnMajorityResetWinOnMajorityPlayer1()
{
    winOnMajorityPlayer1Test();
    gm->reset();
    winOnMajorityPlayer1Test();
}

void GameModelTest::undoSimpleTest()
{
    gm->addPointPlayer1();
    QCOMPARE(gm->getPlayer1SetPoints(), 1);
    gm->undo();
    QCOMPARE(gm->getPlayer1SetPoints(), 0);
    gm->addPointPlayer2();
    QCOMPARE(gm->getPlayer2SetPoints(), 1);
    gm->undo();
    QCOMPARE(gm->getPlayer2SetPoints(), 0);
}

void GameModelTest::undoWinTest()
{
    incrementPoints(WHEN_SET_END, PlayerIncrement::Both);

    gm->addPointPlayer1();
    gm->addPointPlayer1();
    QCOMPARE(gm->getPlayer1SetPoints(), WHEN_SET_END + 2);
    VERIFY_PLAYER_1_WIN();

    gm->undo();
    QCOMPARE(gm->getPlayer1SetPoints(), WHEN_SET_END + 1);
    VERIFY_PLAYER_1_WIN(false);

    gm->addPointPlayer1();
    QCOMPARE(gm->getPlayer1SetPoints(), WHEN_SET_END + 2);
    VERIFY_PLAYER_1_WIN();

    gm->undo();
    QCOMPARE(gm->getPlayer1SetPoints(), WHEN_SET_END + 1);
    VERIFY_PLAYER_1_WIN(false);
    QCOMPARE(gm->getPlayer2SetPoints(), WHEN_SET_END);
    gm->addPointPlayer2();
    VERIFY_PLAYER_2_WIN(false);
    gm->addPointPlayer2();
    VERIFY_PLAYER_2_WIN(false);
    gm->addPointPlayer2();
    QCOMPARE(gm->getPlayer2SetPoints(), WHEN_SET_END + 3);
    VERIFY_PLAYER_2_WIN();
}

void GameModelTest::undoToPreviousSetTest()
{
    incrementPoints(WHEN_SET_END, PlayerIncrement::Player1);
    QCOMPARE(gm->getPlayer1SetPoints(), WHEN_SET_END);
    QCOMPARE(gm->getPlayer2SetPoints(), 0);
    QCOMPARE(gm->getPlayer1MatchScore(), 0);
    QCOMPARE(gm->getPlayer2MatchScore(), 0);

    gm->startNewSet();
    QCOMPARE(gm->getPlayer1SetPoints(), 0);
    QCOMPARE(gm->getPlayer2SetPoints(), 0);
    QCOMPARE(gm->getPlayer1MatchScore(), 1);
    QCOMPARE(gm->getPlayer2MatchScore(), 0);

    gm->undo();
    QCOMPARE(gm->getPlayer1SetPoints(), WHEN_SET_END - 1);
    QCOMPARE(gm->getPlayer2SetPoints(), 0);
    QCOMPARE(gm->getPlayer1MatchScore(), 0);
    QCOMPARE(gm->getPlayer2MatchScore(), 0);
}

void GameModelTest::servingPlayer()
{
    QCOMPARE(gm->getServingPlayer(), (int) Player1);
    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player1);

    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player2);
    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player2);

    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player1);
    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player1);

    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player2);
    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player2);
}

void GameModelTest::servingPlayer2()
{
    settings->setServingStartPlayer(2);

    QCOMPARE(gm->getServingPlayer(), (int) Player2);
    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player2);

    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player1);
    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player1);

    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player2);
    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player2);

    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player1);
    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player1);
}

void GameModelTest::servingPlayerPass11()
{
    incrementPoints(WHEN_SET_END, PlayerIncrement::Both);
    QCOMPARE(gm->getServingPlayer(), (int) Player1);
    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player2);
    gm->addPointPlayer2();
    QCOMPARE(gm->getServingPlayer(), (int) Player1);
}

void GameModelTest::servingPlayer2Pass11()
{
    settings->setServingStartPlayer(2);

    incrementPoints(WHEN_SET_END, PlayerIncrement::Both);
    QCOMPARE(gm->getServingPlayer(), (int) Player2);
    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player1);
    gm->addPointPlayer2();
    QCOMPARE(gm->getServingPlayer(), (int) Player2);
}

void GameModelTest::servingPlayerUndo()
{
    QCOMPARE(gm->getServingPlayer(), (int) Player1);
    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player1);

    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player2);
    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player2);

    gm->undo();
    QCOMPARE(gm->getServingPlayer(), (int) Player2);
    gm->undo();
    QCOMPARE(gm->getServingPlayer(), (int) Player1);
    gm->undo();
    QCOMPARE(gm->getServingPlayer(), (int) Player1);
}

void GameModelTest::servingPlayer2Undo()
{
    settings->setServingStartPlayer(2);

    QCOMPARE(gm->getServingPlayer(), (int) Player2);
    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player2);

    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player1);
    gm->addPointPlayer1();
    QCOMPARE(gm->getServingPlayer(), (int) Player1);

    gm->undo();
    QCOMPARE(gm->getServingPlayer(), (int) Player1);
    gm->undo();
    QCOMPARE(gm->getServingPlayer(), (int) Player2);
    gm->undo();
    QCOMPARE(gm->getServingPlayer(), (int) Player2);
}

void GameModelTest::servingPlayerSecondSet()
{
    QCOMPARE(gm->getServingPlayer(), (int) Player1);

    incrementPoints(WHEN_SET_END, PlayerIncrement::Player1);
    gm->startNewSet();

    QCOMPARE(gm->getServingPlayer(), (int) Player2);
}

void GameModelTest::servingPlayer2SecondSet()
{
    settings->setServingStartPlayer(2);
    QCOMPARE(gm->getServingPlayer(), (int) Player2);

    incrementPoints(WHEN_SET_END, PlayerIncrement::Player2);
    gm->startNewSet();

    QCOMPARE(gm->getServingPlayer(), (int) Player1);
}

void GameModelTest::gameEnd_1()
{
    QSignalSpy spy(gm.get(), &GameModel::gameEnded);
    settings->setGameEnd(1);

    incrementPoints(WHEN_SET_END, PlayerIncrement::Player1);
    QCOMPARE(spy.count(), 1);
}

void GameModelTest::gameEnd_3()
{
    QSignalSpy spy(gm.get(), &GameModel::gameEnded);
    settings->setGameEnd(3);

    incrementPoints(WHEN_SET_END, PlayerIncrement::Player1);
    QCOMPARE(spy.count(), 0);
    gm->startNewSet();

    incrementPoints(WHEN_SET_END, PlayerIncrement::Player2);
    QCOMPARE(spy.count(), 0);
    gm->startNewSet();

    incrementPoints(WHEN_SET_END, PlayerIncrement::Player1);
    QCOMPARE(spy.count(), 1);
}

void GameModelTest::resetSet()
{
    gm->resetSet();
    QCOMPARE(gm->getPlayer1SetPoints(), 0);
    QCOMPARE(gm->getPlayer2SetPoints(), 0);
    QCOMPARE(gm->getPlayer1MatchScore(), 0);
    QCOMPARE(gm->getPlayer2MatchScore(), 0);

    incrementPoints(2, PlayerIncrement::Player1);
    incrementPoints(3, PlayerIncrement::Player2);
    gm->resetSet();
    QCOMPARE(gm->getPlayer1SetPoints(), 0);
    QCOMPARE(gm->getPlayer2SetPoints(), 0);
    QCOMPARE(gm->getPlayer1MatchScore(), 0);
    QCOMPARE(gm->getPlayer2MatchScore(), 0);

    incrementPoints(WHEN_SET_END, PlayerIncrement::Player1);
    gm->resetSet();
    QCOMPARE(gm->getPlayer1SetPoints(), 0);
    QCOMPARE(gm->getPlayer2SetPoints(), 0);
    QCOMPARE(gm->getPlayer1MatchScore(), 0);
    QCOMPARE(gm->getPlayer2MatchScore(), 0);

    incrementPoints(WHEN_SET_END, PlayerIncrement::Player1);
    gm->startNewSet();
    incrementPoints(3, PlayerIncrement::Player2);
    gm->resetSet();
    QCOMPARE(gm->getPlayer1SetPoints(), 0);
    QCOMPARE(gm->getPlayer2SetPoints(), 0);
    QCOMPARE(gm->getPlayer1MatchScore(), 1);
    QCOMPARE(gm->getPlayer2MatchScore(), 0);

    incrementPoints(WHEN_SET_END, PlayerIncrement::Player2);
    gm->startNewSet();
    incrementPoints(3, PlayerIncrement::Player1);
    gm->resetSet();
    QCOMPARE(gm->getPlayer1SetPoints(), 0);
    QCOMPARE(gm->getPlayer2SetPoints(), 0);
    QCOMPARE(gm->getPlayer1MatchScore(), 1);
    QCOMPARE(gm->getPlayer2MatchScore(), 1);
}

void GameModelTest::reset()
{
    gm->reset();
    QCOMPARE(gm->getPlayer1SetPoints(), 0);
    QCOMPARE(gm->getPlayer2SetPoints(), 0);
    QCOMPARE(gm->getPlayer1MatchScore(), 0);
    QCOMPARE(gm->getPlayer2MatchScore(), 0);

    incrementPoints(WHEN_SET_END, PlayerIncrement::Player1);
    gm->reset();
    QCOMPARE(gm->getPlayer1SetPoints(), 0);
    QCOMPARE(gm->getPlayer2SetPoints(), 0);
    QCOMPARE(gm->getPlayer1MatchScore(), 0);
    QCOMPARE(gm->getPlayer2MatchScore(), 0);

    incrementPoints(WHEN_SET_END, PlayerIncrement::Player1);
    gm->startNewSet();
    incrementPoints(3, PlayerIncrement::Player2);
    gm->reset();
    QCOMPARE(gm->getPlayer1SetPoints(), 0);
    QCOMPARE(gm->getPlayer2SetPoints(), 0);
    QCOMPARE(gm->getPlayer1MatchScore(), 0);
    QCOMPARE(gm->getPlayer2MatchScore(), 0);
}
