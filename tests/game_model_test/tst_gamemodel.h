/*
 * Copyright (c) 2019 Paweł Rogoż.
 *
 * This file is part of PingPongXCounter 
 * (see https://gitlab.com/PeeR77/pingpongxcounter).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TST_GAMEMODELTEST_H
#define TST_GAMEMODELTEST_H

#include <QString>
#include <QtTest>
#include <memory>

#include "gamemodel.h"

class GameModelTest : public QObject
{
    Q_OBJECT

private:
    const int WHEN_SET_END = 11;
    enum class PlayerIncrement { Player1, Player2, Both };
    void incrementPoints(int points, PlayerIncrement incrementMode);
    void VERIFY_PLAYER_1_WIN(bool reallyWin = true);
    void VERIFY_PLAYER_2_WIN(bool reallyWin = true);

private Q_SLOTS:
    void init()
    {
        settings->setDefaultSettings();
        gm = std::make_unique<GameModel>();
    }

    void player1Win11To0Test();
    void player2Win11To0Test();
    void SetPlayer1_11ToPlayer2_10Test();
    void SetPlayer2_11ToPlayer1_10Test();
    void winOnMajorityPlayer1Test();
    void winOnMajorityPlayer2Test();
    void winOnMajorityWithUndo();
    void winOnMajorityResetWinOnMajorityPlayer1();
    void undoSimpleTest();
    void undoWinTest();
    void undoToPreviousSetTest();
    void servingPlayer();
    void servingPlayer2();
    void servingPlayerPass11();
    void servingPlayer2Pass11();
    void servingPlayerUndo();
    void servingPlayer2Undo();
    void servingPlayerSecondSet();
    void servingPlayer2SecondSet();
    void gameEnd_1();
    void gameEnd_3();
    void resetSet();
    void reset();

private:
    std::unique_ptr<GameModel> gm;
    Settings *settings = Settings::getInstance();
};

#endif // TST_GAMEMODELTEST_H
