/*
 * Copyright (c) 2019 Paweł Rogoż.
 *
 * This file is part of PingPongXCounter 
 * (see https://gitlab.com/PeeR77/pingpongxcounter).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "tst_matchhistorybrowser.h"


void MatchHistoryBrowserTest::browseSetIndexChanged()
{
    QSignalSpy spy(m_browser.get(), &MatchHistoryBrowser::browseSetIndexChanged);

    int index = 1;
    m_browser->setBrowseSetIndex(index);
    QCOMPARE(spy.count(), 1);

    QList<QVariant> arguments = spy.takeFirst();
    QVERIFY(arguments.at(0).toInt() == index);

    spy.clear();
    m_browser->setBrowseSetIndex(index);
    QCOMPARE(spy.count(), 0);
}

void MatchHistoryBrowserTest::setIndexToCurrentSet()
{
    QSignalSpy spy(m_browser.get(), &MatchHistoryBrowser::browseSetIndexChanged);

    int index = 1;
    m_browser->setBrowseSetIndex(index);
    QCOMPARE(spy.count(), 1);

    QList<QVariant> arguments = spy.takeFirst();
    QVERIFY(arguments.at(0).toInt() == index);

    spy.clear();
    m_browser->setIndexToCurrentSet();
    QCOMPARE(spy.count(), 1);

    arguments = spy.takeFirst();
    QVERIFY(arguments.at(0).toInt() == 0);
}

void MatchHistoryBrowserTest::getPlayer1SetPoints()
{
    testGetPlayerPoints(std::bind(&MatchHistoryBrowser::getPlayer1SetPoints, m_browser.get()), PlayerIncrement::Player1);
}

void MatchHistoryBrowserTest::getPlayer2SetPoints()
{
    testGetPlayerPoints(std::bind(&MatchHistoryBrowser::getPlayer2SetPoints, m_browser.get()), PlayerIncrement::Player2);
}

void MatchHistoryBrowserTest::getPlayer1SetHistory()
{
    testGetPlayerSetHistory(std::bind(&MatchHistoryBrowser::getPlayer1SetHistory, m_browser.get()), PlayerIncrement::Player1);
}

void MatchHistoryBrowserTest::getPlayer2SetHistory()
{
    testGetPlayerSetHistory(std::bind(&MatchHistoryBrowser::getPlayer2SetHistory, m_browser.get()), PlayerIncrement::Player2);
}

void MatchHistoryBrowserTest::incrementPoints(int points, MatchHistoryBrowserTest::PlayerIncrement incrementMode)
{
    for (int i = 0; i < points; i++) {
        if (incrementMode == PlayerIncrement::Player1 || incrementMode == PlayerIncrement::Both)
            m_gameData->getCurrentSet().addPoint(Player1);

        if (incrementMode == PlayerIncrement::Player2 || incrementMode == PlayerIncrement::Both)
            m_gameData->getCurrentSet().addPoint(Player2);
    }
}

void MatchHistoryBrowserTest::testGetPlayerPoints(std::function<int()> testingFunction, MatchHistoryBrowserTest::PlayerIncrement playerIncrement)
{
    QCOMPARE(testingFunction(), 0);

    int pointsSet1 = 3;
    incrementPoints(pointsSet1, playerIncrement);
    QCOMPARE(testingFunction(), pointsSet1);

    // Check set whitch not yet exist
    m_browser->setBrowseSetIndex(1);
    QCOMPARE(testingFunction(), 0);

    m_gameData->startNewSet();
    QCOMPARE(testingFunction(), 0);

    int pointsSet2 = 5;
    incrementPoints(pointsSet2, playerIncrement);
    QCOMPARE(testingFunction(), pointsSet2);

    m_browser->setBrowseSetIndex(0);
    QCOMPARE(testingFunction(), pointsSet1);

    m_browser->setBrowseSetIndex(1);
    QCOMPARE(testingFunction(), pointsSet2);
}

void MatchHistoryBrowserTest::testGetPlayerSetHistory(std::function<QVector<bool>()> testingFunction, MatchHistoryBrowserTest::PlayerIncrement playerIncrement)
{
    auto player1History = testingFunction();
    QCOMPARE(player1History.count(), 11);
    for(auto& item : player1History)
        QCOMPARE(item, false);

    incrementPoints(10, playerIncrement);
    player1History = testingFunction();
    for (int i = 0; i < player1History.size() - 1; i++)
        QCOMPARE(player1History[i], true);
    QCOMPARE(player1History.last(), false);

    incrementPoints(3, playerIncrement == PlayerIncrement::Player1 ?
                    PlayerIncrement::Player2 : PlayerIncrement::Player1);
    player1History = testingFunction();
    QCOMPARE(player1History.count(), 11+2);
    for (int i = 0; i < 10; i++)
        QCOMPARE(player1History[i], true);
    for (int i = 10; i < player1History.size(); i++)
        QCOMPARE(player1History[i], false);

    m_gameData->startNewSet();
    m_browser->setBrowseSetIndex(1);
    incrementPoints(3, playerIncrement);
    player1History = testingFunction();
    QCOMPARE(player1History.count(), 11);
    QCOMPARE(player1History[2], true);
    QCOMPARE(player1History[3], false);

    m_browser->setBrowseSetIndex(0);
    player1History = testingFunction();
    for (int i = 0; i < 10; i++)
        QCOMPARE(player1History[i], true);
    QCOMPARE(player1History.last(), false);
}
