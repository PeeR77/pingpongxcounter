/*
 * Copyright (c) 2019 Paweł Rogoż.
 *
 * This file is part of PingPongXCounter 
 * (see https://gitlab.com/PeeR77/pingpongxcounter).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TST_MATCHHISTORYBROWSER_H
#define TST_MATCHHISTORYBROWSER_H

#include <QtTest>

#include "matchhistorybrowser.h"


class MatchHistoryBrowserTest : public QObject
{
    Q_OBJECT

private slots:
    void init() {
        m_gameData = std::make_unique<GameData>();
        m_browser = std::make_unique<MatchHistoryBrowser>(*m_gameData);
    }

    void browseSetIndexChanged();
    void setIndexToCurrentSet();

    void getPlayer1SetPoints();
    void getPlayer2SetPoints();
    void getPlayer1SetHistory();
    void getPlayer2SetHistory();

private:
    enum class PlayerIncrement { Player1, Player2, Both };
    void incrementPoints(int points, PlayerIncrement incrementMode);
    void testGetPlayerPoints(std::function<int()> testingFunction, PlayerIncrement playerIncrement);
    void testGetPlayerSetHistory(std::function<QVector<bool>()> testingFunction, PlayerIncrement playerIncrement);

private:
    std::unique_ptr<GameData> m_gameData;
    std::unique_ptr<MatchHistoryBrowser> m_browser;
};

#endif // TST_MATCHHISTORYBROWSER_H
